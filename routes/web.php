<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Non auth routes - opening page and public questionnaire view
Route::get('/', function () {
    return view('welcome');
});

Route::get('/respondent/home', 'HomeController@index')->name('home');

// Auth wrapped around all routes to be protected by password.
Auth::routes();
Route::group(['middleware' => ['web']], function() {
    /**
     * name routes used to reduce code when adding route paths
     */
    Route::get('/researchers/dashboard', 'DashboardController@index')->name('dashboard');

    // Questionnaire route request targetting the questionnaire controller at the required methods.
    // Route hits the create method and displays form to create a questionnaire.
    Route::get('/researchers/questionnaires/create', 'QuestionnaireController@create')->name('create_questionnaire');

    // Route hits the store method for validation and saving via the user to the database. Flash shown.
    Route::post('/researchers/questionnaires', 'QuestionnaireController@store')->name('new_questionnaire');

    // Route hits the show method to display questionnaires - question, answer and response data also lazy loaded.
    Route::get('/researchers/questionnaires/{questionnaire}', 'QuestionnaireController@show')->name('show_questionnaire');

    // Route hits the edit method to display the pre-populated form for updating a questionnaire.
    Route::get('/researchers/questionnaires/{questionnaire}/edit', 'QuestionnaireController@edit')->name('edit_questionnaire');

    // Route hits the update method to valid the update form and then saving new data to DB.
    Route::patch('researchers/questionnaires/{questionnaire}', 'QuestionnaireController@update')->name('update_questionnaire');

    // Route hits the destroy method to delete all questionnaires and its related questions and answers.
    Route::delete('/researchers/questionnaires/{questionnaire}', 'QuestionnaireController@destroy')->name('delete_questionnaire');
    
    // Question route requests targetting question controller at required method.

    // Route hit the create method to display the form for creating a new question.
    Route::get('/researchers/questionnaires/{questionnaire}/questions/create', 'QuestionController@create')->name('create_question');

    // Route hits the store method for validation and saves question and answers via relationsip to DB
    Route::post('/researchers/questionnaires/{questionnaire}/questions', 'QuestionController@store')->name('new_question');

    // Routes hit destroy method to delete a question and its related answers
    Route::delete('/researchers/questionnaires/{questionnaire}/questions/{question}', 'QuestionController@destroy')->name('delete_question');
    
    // Respondent routes used to handle live questionnaires via survey controller

    // Route hits show method to display questionnaires and its related questions and answers for completion.
    Route::get('/surveys/{questionnaire}', 'SurveyController@show')->name('show_survey');

    // Route hits store mehod for validation and to save the survey and its reponses to the DB
    Route::post('/surveys/{questionnaire}', 'SurveyController@store')->name('new_survey');

 });

// Routes for editing and updating questions if functionality was added.
// Route::get('/researchers/questionnaires/{questionnaire}/questions/{question}/edit', 'QuestionController@edit')->name('edit_question');
// Route::patch('/researchers/questionnaires/{questionnaire}/questions', 'QuestionController@update')->name('update_question');
