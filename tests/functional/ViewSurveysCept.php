<?php 
$I = new FunctionalTester($scenario);

$I->am('respondent');
$I->wantTo('view questionnaires');

//Questionnaire records to display
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'active' => '1',
    'user_id' => '666', 
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
]);

$I->haveRecord('questionnaires',[
    'id' => '6005',
    'active' => '1',
    'user_id' => '666', 
    'title' => 'Questionnaire 2',
    'description' => 'Questionnaire 2 description',
]);

$I->seeRecord('questionnaires', ['id' => '6000', 'title' => 'Questionnaire 1']);
//And
$I->seeRecord('questionnaires', ['id' => '6005', 'title' => 'Questionnaire 2']);


//When
$I->amOnPage('/respondent/home');

//And
$I->see('Public Surveys');
$I->see('Questionnaire 1');
$I->see('Questionnaire 2');
