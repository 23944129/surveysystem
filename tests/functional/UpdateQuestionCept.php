<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('update a question');

Auth::loginUsingId(1);


//Add test questionnaire to show that content is being listed
$I->haveRecord('questionnaires',[
    'id' => '6006',
    'active' => '0',
    'user_id' => '1',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    
]);

$I->haveRecord('questions', [
    'id' => '2000',
    'questionnaire_id' => '6006',
    'question' => 'Question 1',
    
]);

$I->seeRecord('questions',['questionnaire_id' => '6006', 'id'=> '2000']);

//When
$I->amOnPage('/researchers/questionnaires/6006');
$I->see('Questionnaire 1');
$I->see('Question 1');

//Then
$I->click('Update Question');

//And
$I->amOnPage('/researchers/questionnaires/6006/questions/2000/edit');
$I->see('Update Question');
$I->see('Question 1');


//Then
$I->fillField('question', 'Updated question');

//And
$I->click('Update Question');

//Then 
$I->seeCurrentUrlEquals('researchers/questionnaires/6006');

//And
$I->See('Updated Question');
