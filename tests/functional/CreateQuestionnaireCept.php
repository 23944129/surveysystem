<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new questionnaire');

Auth::loginUsingId(1);

//Add test questionnaire to show that content is being added to db
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'active' => '0',
    'user_id' => '666', 
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
]);
 
//When
$I->amOnPage('/researchers/dashboard');
$I->see('Your Questionnaires');

//And
$I->click('Create Questionnaire');

//Then
$I->amOnPage('/researchers/questionnaires/create');

//And
$I->see('Create New Questionnaire', 'h3');

$I->submitForm('#createQuestionnaire', [
    'title' => 'Questionnaire 2',
    'description' => 'Questionnaire 2 description',
    'active' => '1',
]);

//When
$I->amOnPage('/researchers/dashboard');
$I->see('Questionnaire 2');

