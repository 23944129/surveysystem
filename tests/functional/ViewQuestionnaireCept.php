<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('view a questionnaire');


Auth::loginUsingId(1);

//Questionnaire records to display
$I->haveRecord('questionnaires',[
    'id' => '6006',
    'active' => '0',
    'user_id' => '1',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
]);

$I->haveRecord('questionnaires',[
    'id' => '6007',
    'active' => '0',
    'user_id' => '1',
    'title' => 'Questionnaire 2',
    'description' => 'Questionnaire 2 description',
]);


$I->seeRecord('questionnaires', ['id' => '6006', 'title' => 'Questionnaire 1']);
$I->seeRecord('questionnaires', ['id' => '6007', 'title' => 'Questionnaire 2']);

//When
$I->amOnPage('/researchers/dashboard');

//And
$I->see('Your Questionnaires');
$I->see('Questionnaire 1');
$I->see('Questionnaire 2');

//And
$I->click('Questionnaire 1');

//Then
$I->seeCurrentUrlMatches('~/researchers/questionnaires/(\d+)~');
$I->see('Questionnaire 1', 'h3');
