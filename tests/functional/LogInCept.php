<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('log in to account');


//Add test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password'
]);

$I->seeRecord('users', ['email' => 'fake@email.com', 'password' => 'password']);

//When
$I->amOnPage('/login');

//And
$I->see('Login');

//And
$I->dontSee('You are logged in!');

$I->submitForm('#login', [
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Then
$I->amOnPage('/researchers/dashboard');

//And
$I->see('Dashboard');