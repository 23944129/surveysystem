<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('update a questionnaire');

Auth::loginUsingId(1);

//Add test questionnaire to show that content is being added to db
$I->haveRecord('questionnaires',[
    'id' => '6006',
    'active' => '0',
    'user_id' => '1',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
]);

//Record is accessible
$I->seeRecord('questionnaires',[
    'id' => '6006',
    'title' => 'Questionnaire 1',
]);

//When
$I->amOnPage('/researchers/dashboard');

//And
$I->see('Your Questionnaires');
$I->see('Questionnaire 1');

//And
$I->click('Questionnaire 1');

//Then
$I->amOnPage('/researchers/questionnaires/6006');

//And
$I->see('Questionnaire 1', 'h3');

//And
$I->click('Update Questionnaire');

$I->amOnpage('/researchers/questionnaires/6006/edit');

//And
$I->see('Update Questionnaire', 'h3');

//Then
$I->submitForm('#updateQuestionnaire', [
    'title' => 'Updated Title',
    'description' => 'Updated description',
    'active' => '1',
]);

//Then 
$I->seeCurrentUrlMatches('#researchers/questionnaires/6006#');

//And
$I->See('Updated Title');
