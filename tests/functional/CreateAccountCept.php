<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new user account');

//When
$I->amOnPage('/');
$I->see('Register');
$I->dontSee('You are logged in!');

//Then
$I->click('Register');

//Then
$I->amOnPage('/register');

//Then
$I->submitForm('#register', [
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
    'password-confirm' => 'password',
]);

//Then
$I->amOnPage('/home');