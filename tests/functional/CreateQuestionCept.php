<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new question');


Auth::loginUsingId(1);

//Add test questionnaire to show that content is added to db
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'active' => '0',
    'user_id' => '1',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
     
]);

//Add test question to show content being added to db
$I->haveRecord('questions',[
    'id' => '6002',
    'questionnaire_id' => '6000',
    'question' => 'How are you?'
]);

$I->seeRecord('questions', ['question' => 'How are you?']);

//When
$I->amOnPage('/researchers/questionnaires/6000');
$I->see('Questionnaire 1', 'h3');

// //And
 $I->click('Add Question');

 //Then
 $I->seeCurrentUrlMatches('~/researchers/questionnaires/(\d+)/questions/create~');

// //And
$I->see('Create New Question', 'h3');

//Then
$I->submitForm('#createQuestion', [
    'question' => 'Question 1',
    'answer1' => 'Answer 1',
    'answer2' => 'Answer 2',
    'answer3' => 'Answer 3',
    'answer4' => 'Answer 4',
    'answer5' => 'Answer 5', 
 ]);

$I->seeRecord('questions', ['question' => 'Question 1']);
// //Then
$I->seeCurrentUrlMatches('#researchers/questionnaires/6000#');
$I->see('Questionnaire 1', 'h3');

//And
$I->see('Question 1');
