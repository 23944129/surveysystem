<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('remove a questionnaire');

Auth::loginUsingId(1);

//Add db test user
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Add test questionnaire to show that content is being listed
$I->haveRecord('questionnaires',[
    'id' => '6006',
    'active' => '0',
    'user_id' => '1',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description', 
]);

$I->haveRecord('questions',[
    'id' => '6002',
    'questionnaire_id' => '6000',
    'question' => 'How are you?'
]);

$I->seeRecord('questions', ['question' => 'How are you?']);
$I->seeRecord('questionnaires',[
    'id' => '6006',
    'title' => 'Questionnaire 1',
]);

//When
$I->amOnPage('/researchers/dashboard');

//And
$I->see('Your Questionnaires');
$I->see('Questionnaire 1');

//And
$I->click('Questionnaire 1');

//Then
$I->amOnPage('/researchers/questionnaires/6006');

//And
$I->see('Questionnaire 1', 'h3');

//And
$I->click('Delete Questionnaire');

//Then 
$I->amOnPage('/researchers/dashboard');

//And
$I->see('Your Questionnaires');
$I->dontSee('Questionnaire 1');
