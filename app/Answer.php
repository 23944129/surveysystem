<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    // turns on mass assignment for answer field
    protected $fillable = [
        'answer'
    ];
/**
 *  Defing the relationships for answer to the other classes
 */
    // an answer belongs to a question
     public function question()
    {
        return $this->belongsTo(Question::class);
    }

    // an answer belongs to a questionnaire
    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class);
    }

    // an answer can have many responses
    public function responses()
    {
        return $this->hasMany(Response::class);
    }
}
