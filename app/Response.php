<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    // allows mass assignment
    protected $guarded = [];


    /**
     * define survey relationships
     */
    // a response belongs to a survey
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
}
