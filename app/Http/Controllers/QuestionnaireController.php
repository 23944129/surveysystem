<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Question;
use App\Answer;
use Session;

class QuestionnaireController extends Controller
{   /*
        Auth constructor to allow questionnaire methods to be protected by password
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /** 
     * returns create questionnaire view in researcher directory with questionnaire route/model binding.
    */    
     public function create(Questionnaire $questionnaire)
    {
        return view('researcher.questionnaire.create', compact('questionnaire'));
    }

    // validates requested input for validation feedback to user for form.
    public function store()
    {
        $data = request()->validate([
            'title' => 'bail|required|min:3|max:255',
            'description' => 'required|min:3|max:255',
            'active' => 'required',
        ]);

        /**
         * using auth finds the user, then their questionnaires and creates data to store
         */
        $questionnaire = auth()->user()->questionnaires()->create($data);

        // session data used to user when questionnaire added
        Session::flash('flash_message', 'Questionnaire added!');
        return redirect(route('dashboard'));
    }

    public function show(Questionnaire $questionnaire)
    {   
        //lazy loading nested data via relationships from models
        $questionnaire->load('questions.answers.responses');

        return view('researcher.questionnaire.show', compact('questionnaire'));
    }

    public function edit($id)
    {
        /**
         * finds questionnaire via its id to return the edit form for the correct one
         */
        $questionnaire = Questionnaire::findOrFail($id);

        return view('researcher.questionnaire.edit', compact('questionnaire'));
    }

    /**
     * validatin applied the same as creating a questionnaire
     * 
     * finds a the questionniare by id then updates details via the update request
     */
    public function update(Request $request, $id)
    {
        $data = request()->validate([
            'title' => 'bail|required|min:3|max:255',
            'description' => 'required|min:3|max:255',
            'active' => 'required',
        ]);

        $questionnaire = questionnaire::findOrFail($id);
        $questionnaire->update($request->all());

        return redirect('/researchers/questionnaires/'.$questionnaire->id);
    }

    /**
     * route model binding and relationships used to delete nested data.
     * first deletes the answers then works back to delete questions and questionnaires.
     */
    public function destroy(Questionnaire $questionnaire, Question $question, Answer $answer)
    {
        $questionnaire->answers()->delete();
        $questionnaire->questions()->delete();
        $questionnaire->delete();

        Session::flash('flash_message1', 'Questionnaire deleted!');
        return redirect('/researchers/dashboard');
    }

}
