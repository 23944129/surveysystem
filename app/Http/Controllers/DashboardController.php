<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;

class DashboardController extends Controller
{   
    // assigns auth middleware to controller methods via contructor
    public function __construct()
    {
        $this->middleware('auth');
    }
    /** 
     * Display a listing of researchers questionnaires on their dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** uses auth to display only the logged in users questionnaire
         *      back to them on their dashboard
         * 
         *  variable used to hold the logged in users specific questionnaires
         *  then passed to view
         *  */  
        
        $questionnaires = auth()->user()->questionnaires;

        return view('researcher.dashboard', compact('questionnaires'));
    }

}
