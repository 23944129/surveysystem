<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the respondent non logged in view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /**
     * index method takes all questionnaires and makes them
     *  available to the reposndent non logged in view
     * 
     */
    public function index()
    {
        $questionnaires = Questionnaire::all();

        return view('respondent.home', compact('questionnaires'));
    }
}
