<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Question;
use Session;


class QuestionController extends Controller
{
    /**
     *  create method returns the create form view.
     *  nested route/model binding to retreive questionnaires and questions 
     */
    public function create(Questionnaire $questionnaire, Question $question)
    {
        return view('researcher.question.create', compact('questionnaire', 'question'));
    }

    
    public function store(Questionnaire $questionnaire)
    {
        /**
         * validates form entried as required so empty form cannot be submitted.
         *  arrays used for question and answers so dot notation used in validation to access all.
         *   * wild card accesses all answers in array
         */
        $data = request()->validate([
            'question.question' => 'bail|required|min:3|max:255',
            'choices.*.answer' => 'required|max:255',
        ]);

        /**
         *  saving through relationships, takes all a questionnaires questions and creates data
         *  via question array.
         *  same for a question having many answers in an array, using create many from choices
         *  array
         */
        $question = $questionnaire->questions()->create($data['question']);
        $question->answers()->createMany($data['choices']);

        // Session data used to display message to user when question added.
        Session::flash('flash_message', 'Question added!');
        return redirect('/researchers/questionnaires/'.$questionnaire->id);
    }

    // Double route/model binding to delete via relationships
    public function destroy(Questionnaire $questionnaire, Question $question)
    {
        /**
         * deleting via relationship as done with creating.
         *  finds all of a questions answers and deletes them
         *  then deletes the question.
         */
        $question->answers()->delete();
        $question->delete();

        // session data used to display message when a question is successfully deleted.
        Session::flash('flash_message1', 'Question deleted!');
        return redirect('/researchers/questionnaires/'.$questionnaire->id);
    }

}
