<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Question;
use App\Survey;
use Session;

class SurveyController extends Controller
{
    /**
     * lazy loading data to show respondents a survey to complete
     * passing questionnaire, then loading all of its associated questions and their answers
     */
    public function show(Questionnaire $questionnaire)
    {
        $questionnaire->load('questions.answers');

        return view('respondent.show', compact('questionnaire'));
    }

    /**
     * Questionnaire used via route/model binding
     * arrays used for reponses to collect answer and question data
     * consent form data collected in array to submit (email is optional).
    */
     public function store(Questionnaire $questionnaire)
    {
        $data = request()->validate([
            'responses.*.answer_id' => 'required',
            'responses.*.question_id' => 'required',
            'consent.ethic_agree' => 'accepted',
            'consent.email' => 'nullable|email',
        ]);
    
        // storing data via relationships using a questionnaires survey to add the survey array data
        $survey = $questionnaire->surveys()->create($data['consent']);

        // a survey having many responses used to create response data via responses arrays and question/answer id's
        $survey->responses()->createMany($data['responses']);
        
        // used to error catch info passsing to db
             //dd(request()->all());

        // session data used to provide response feedback to user on submission
        Session::flash('flash_message', 'Response Submitted. Thank you!!');
        return redirect(route('home'));
    }
}
