<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    // allows mass assignment for the named form fields in array 
    protected $fillable = [
        'title',
        'description',
        'active',
    ];

/**
 * Defining the questionnaire relationships to other classes
 */
    // a questionnaire belongs to a user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // a questionnaire has many questions
     public function questions()
    {
        return $this->hasMany(Question::class);
    }

    // a questionnaire has many surveys
    public function surveys()
    {
        return $this->hasMany(Survey::class);
    }

    // a questionnaire herits answers through questions
    public function answers()
    {
        return $this->hasManyThrough(Answer::class, Question::class);
    }
   
}



