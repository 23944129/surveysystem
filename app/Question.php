<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    // mass assignment turns on for creating question (answers being part of the question)
    protected $fillable = [
        'question',
        'answer1',
        'answer3',
        'answer4',
        'answer5',
    ];
/**
 *  Defining the question relationships to other classes
 */
     // a question has many answers
     public function answers()
     {
         return $this->hasMany(Answer::class);
     }

    // a question belongs to a questionnaire
    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class);
    }

    // a question has many responses
    public function responses()
    {
        return $this->hasMany(Response::class);
    }
}
