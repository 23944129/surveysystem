<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    // allows mass assignment of form fields.
    protected $guarded = [];
    

/**
 * define questionnaire relationships
 */
    // a survey belongs to a questionnaire
    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class);
    }

    // a survey has many responses
    public function responses()
    {
        return $this->hasMany(Response::class);
    }
    
}
