<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        
            <div class="card shdaow-sm">
                <div class="card-header text-center">
                    <h3>Public Surveys</h3>
                </div>

                <div class="card-body">
                   
                   <ul class="list-group ml-0">
                        @forelse($questionnaires as $questionnaire)
                            @if ($questionnaire->active == '1')
                                <div class="card mb-2">
                                    <div class="card-header">
                                        <strong>{{ $questionnaire->title }}</strong>
                                    </div>
                        
                                    <div class="card-body d-flex justify-content-between align-items-center">
                                        <p class="mb-1 col-9 pl-0">{{ $questionnaire->description }}</p>
                                        <a class="btn btn-sm btn-dark col-3 shadow-sm" href="{{ route('show_survey', $questionnaire->id) }}">Take Survey</a>
                                    </div>
                                </div>  
                            @endif
                        @empty
                            <p class="text-danger text-center">Sorry - no surveys available yet</p>
                        @endforelse
                   </ul>
                </div><!-- Card body -->
            </div><!-- Card -->
        </div><!-- Col -->
    </div><!-- Row -->
</div><!-- Container -->
@endsection
