<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="text-center">{{ $questionnaire->title }}</h1>

            <!-- include used to inject ethics statement to reduce code on page -->
            @include('partials.ethics')

            <!-- if statement to catch form errors using session data -->
            @if ($errors->any())
                    <div>
                        <ul class="alert alert-danger ml-0">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <!-- form using named route to hit store method in survey controller -->
            <form action="{{ route('new_survey', $questionnaire->id) }}" method="post">
                @csrf

                @foreach($questionnaire->questions as $key => $question)
               
                    <div class="card mb-2">
                        <div class="card-header d-flex">
                            <div><badge class="badge badge-dark mr-2 shadow-sm">{{ $key + 1}}</badge></div>
                            <div><h5>{{ $question->question }}</h5></div>
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                @foreach($question->answers as $answer)
                                    <label for="answer{{ $answer->id }}">
                                        <li class="list-group-item">
                                            <input type="radio" name="responses[{{ $key }}][answer_id]" id="answer{{ $answer->id}}" value="{{ $answer->id }}"/>
                                            
                                            {{ $answer->answer }}
                                            <input type="hidden" name="responses[{{ $key }}][question_id]" value="{{ $question->id }}">
                                        </li>
                                    </label>
                                @endforeach
                            </ul>
                        </div><!-- Card body -->
                    </div><!-- Card -->
                @endforeach

                @include('partials.consent')
                
            </form>
        </div><!-- Col -->
    </div><!-- Row -->
</div><!-- Container -->
@endsection
