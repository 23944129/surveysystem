<div class="card mb-2">
    <div class="card-header">
        <h5>Data Protection</h5>
    </div>
    <div class="card-body">
        <p>We will only use your data for the intended purpose of this survey. <br>
            Following that it will be destroyed appropriately in accordance with GDPR</p>
        <a class="pl-3" href="{{ route('home') }}">I've changed my mind - Return home</a>
    </div>            
</div>