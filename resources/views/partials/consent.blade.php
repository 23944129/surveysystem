<div class="card mb-4">
    <div class="card-body">
        <h3 class="card-title">Consent</h3>
        <h6 class="card-subtitle mb-2 text-muted">You may only submit your information by agreeing below:</h6>
        <!-- save form inputs to an array (consent) to pass through values to db when storing data -->
        <div class="card-body">
        <!-- checkbox used to overide default value in db for obtaining consent to use respondent information -->
            <div class="form-group d-flex mb-3">
                <label class=" ml-3 pb-2" for="ethic_agree">I consent for my answers to be used for the purpose of this survey only</label>
                <input type="checkbox" class="form-check-input" name="consent[ethic_agree]" id="ethic_agree" value="1"/> 
            </div>
            <!-- optional email field set to nullable in validation & db so it can be left blank if the respondent choses -->
            <div class="form-group row">
                <p>Enter your email below if you want more information on how we handle your data</p>
                <div class="col-sm-10">
                    <input type="email" class="form-control rounded shadow-sm" name="consent[email]" id="email" placeholder="Email">
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-between">
            <button class="btn btn-dark mb-0 shadow-sm" type="submit">Submit Survey Answers</button>
            <a class="pl-3" href="{{ route('home') }}">I've changed my mind - Return home</a>
        </div> 
    </div><!-- Card body -->
</div><!-- card -->