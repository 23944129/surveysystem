<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-4">
                <div class="card-header text-center">
                    <h3>Create New Question</h3>
                </div>
                <div class="card-body pb-1">
                    <!-- if statement used to list any errors in the session data -->
                    @if ($errors->any())
                        <div>
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <!-- Form inputs use arrays to input question and answer into the db column of that name -->
                    <!-- Form action via named route with questionnaire id passed in -->
                    {{ Form::open([ 'route' => ['new_question',  $questionnaire->id], 'id' => 'createQuestion' ]) }}
                    <!-- security token to prevent cross site request forgery attacks -->
                    @csrf
                        <div class="form-group">
                            {!! Form::label('question', 'Enter a question:', ['class' => 'mb-2']) !!}
                            {!! Form::text('question', null , ['class' => 'form-control rounded shadow-shm_put_var', 'name' => 'question[question]', 'id' => 'question']) !!}
                        </div>
                        <h4>Answer options:</h4>
                        <div class="form-group">
                            {!! Form::label('answer1', 'Enter the first answer choice:', ['class' => 'mb-2']) !!}
                            {!! Form::text('answer1', null, ['class' => 'form-control rounded shadow-shm_put_var', 'name' => 'choices[][answer]', 'id' => 'answer1']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('answer2', 'Enter the second answer choice:', ['class' => 'mb-2']) !!}
                            {!! Form::text('answer2', null, ['class' => 'form-control rounded shadow-shm_put_var', 'name' => 'choices[][answer]', 'id' => 'answer2']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('answer3', 'Enter the third answer choice:', ['class' => 'mb-2']) !!}
                            {!! Form::text('answer3', null, ['class' => 'form-control rounded shadow-shm_put_var', 'name' => 'choices[][answer]', 'id' => 'answer3']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('answer4', 'Enter the fourth answer choice:', ['class' => 'mb-2']) !!}
                            {!! Form::text('answer4', null, ['class' => 'form-control rounded shadow-shm_put_var', 'name' => 'choices[][answer]', 'id' => 'answer4']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('answer5', 'Enter the fifth answer choice:', ['class' => 'mb-2']) !!}
                            {!! Form::text('answer5', null, ['class' => 'form-control rounded shadow-sm', 'name' => 'choices[][answer]', 'id' => 'answer5']) !!}
                        </div>
                        <div>
                            {!! Form::submit('Add Question', ['class' => 'btn btn-success btn-sm shadow-sm rounded mb-3']) !!}
                        </div>
                    {{ Form ::close() }}
                </div><!-- Card body -->
            </div><!-- Card -->
        </div><!-- Col -->
    </div><!-- Row -->
</div><!-- Container -->
@endsection
