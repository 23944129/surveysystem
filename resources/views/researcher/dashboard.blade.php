<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')
<!-- flash message for adding questionnaire-->
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
<!-- flash message for dleeting questionnaire -->
@if(Session::has('flash_message1'))
    <div class="alert alert-danger">
        {{ Session::get('flash_message1') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="text-center">{{ Auth::user()->name }}'s Dashboard</h1>
            <div class="card mb-2 shadow-sm">
                <div class="card-header d-flex justify-content-between">
                    <h3>Your Questionnaires</h3>
                    <div> 
                        <a href="{{ route('create_questionnaire') }}" class="btn btn-info btn-sm mt-2 shadow-sm">Create Questionnaire</a>
                    </div>
                </div>
                <div class="card-body">
                   <ul class="list-group">
                        @forelse($questionnaires as $questionnaire)
                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <a href="{{ route('show_questionnaire', $questionnaire->id) }}" class="text-monospace pt-2">{{ $questionnaire->title }}</a>
                                    <p> {{ $questionnaire->active == 1 ? 'Active' : 'Inactive' }} </p>
                                </div>
                                <div>
                                    <p>{{ $questionnaire->description }}</p>
                                    {{ Form::model($questionnaire, [ 'route' => ['delete_questionnaire',  $questionnaire->id], 'class' => 'd-flex justify-content-end']) }}
                                        @method('DELETE')
                                        @csrf
                                        {!! Form::submit('Delete Questionanire', ['class' => 'btn btn-danger btn-sm rounded shadow-sm']) !!}
                                    {{ Form ::close() }}
                                </div>
                            </li>
                        @empty
                            <p class="text-danger text-center">No Questionnaires yet</p>
                        @endforelse
                   </ul>
                </div><!-- Card body -->
            </div><!-- Card -->
        </div><!-- Col -->
    </div><!-- Row -->
</div><!-- Container -->

@endsection
