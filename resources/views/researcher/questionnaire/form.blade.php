/**
    *incomplete* this form was going to be used for both create and edit questionnaire but changing the form fields and values
    was more complicated than using the same form twice.
    it as has been into show how it would be implemented. By including a modify index in the data array and then dynamically
    displaying data based on the controller method called.
 */


<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h3>{{ $modify == 1 ? 'Update Questionnaire' : 'Create New Questionnaire'}}</h3>
                </div>
                <div class="card-body">
                <!-- modify index in data array in controllers used to dynamically display 2 forms using 1 template -->
                <!-- inline if statements take different routes or display different data  -->
                    <form action="{{ $modify == 1 ? route('update_questionnaire', $questionnaire->id) : route('new_questionnaire') }}" 
                        method="post" id="{{ $modify == 1 ? 'updateQuestionnaire' : 'createQuestionnaire' }}">
                        @csrf
                        <div class="form-group">
                            <p class="mb-1 text-danger">{{ $modify == 1 ? 'Current title:' : '' }}</p>
                            <h5>{{ $modify == 1 ? ( $questionnaire->title ) : '' }}</h5>
                            <label class="mb-1" for="title">{{ $modify == 1 ? 'New Questionnaire Title' : 'Questionnaire Title' }}</label>
                            <input name="title" type="text" class="form-control mb-3 @error('title') is-invalid @enderror" id="title" 
                                placeholder="Enter Questionnaire Title">
                        </div>
                        <div class="form-group">
                            <div>
                                <p class="mb-1 text-danger">{{ $modify == 1 ? 'Current description:' : '' }}</p>
                                <h5>{{ $modify == 1 ? ( $questionnaire->description ) : '' }}</h5>
                            </div>
                            <label class="mb-1" for="description">{{ $modify == 1 ? 'New Questionnaire Description' : 'Questionnaire Description' }}</label>
                            <input name="description" type="text" class="form-control mb-3 @error('description') is-invalid @enderror" id="description" 
                                placeholder="Enter Questionnaire Description">
                        </div>
                        <div class="form-group">
                            <label for="active" class="form-check-label">
                                <input type="checkbox" name="active" id="active" value="1"/>
                            Check to make Questionnaire active / Blank for in-active
                            <input type="hidden" name="active" id="active" value="0" />
                            </label>
                        </div>
                        <button type="submit" class="btn btn-info mb-0">{{ $modify == 1 ? 'Update Questionnaire' : 'Add Questionnaire' }}</button>
                    </form>  
                </div><!-- card body -->
            </div><!-- card -->
        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->
@endsection
