<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h3>Update Questionnaire</h3>
                </div>
                <div class="card-body">
                <!-- if statement used to list any errors in form filling via session data -->
                    @if ($errors->any())
                        <div>
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <!-- form facade used - named route and questionnaire id passsed in to edit correct item. -->
                    {{ Form::model($questionnaire, [ 'route' => ['update_questionnaire',  $questionnaire->id], 'id' => 'updateQuestionnaire' ]) }}
                        <!-- patch request used to edit data via controller update method. security token included -->
                        <!-- same form fields used as create form to ensure proper data entry -->
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            {!! Form::label('title', 'Questionnaire Title:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control rounded', 'id' => 'title']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Questionnaire Description:') !!}
                            {!! Form::text('description', null, ['class' => 'form-control rounded', 'id' => 'title']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::hidden('active', '0', ['id' => 'active']) !!}
                            {!! Form::checkbox('active', '1', false, ['id' => 'active']) !!}
                            {!! Form::label('active', ' Check for Active / Empty for Inactive ') !!}
                        </div>
                        <div>
                            {!! Form::submit('Update Questionnaire', ['class' => 'btn btn-success btn-sm rounded']) !!}
                        </div>
                    {{ Form ::close() }}
                </div><!-- Card body -->
            </div><!-- Card -->
        </div><!-- Col -->
    </div><!-- Row -->
</div><!-- Container -->
@endsection
