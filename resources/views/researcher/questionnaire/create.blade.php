<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h3>Create New Questionnaire</h3>
                </div>
                <div class="card-body">
                <!-- if statement used to list any errors using session data -->
                    @if ($errors->any())
                        <div>
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <!-- form facade used, named route to questionnaire controller store method-->
                    {{ Form::model($questionnaire, ['route' => 'new_questionnaire', 'id' => 'createQuestionnaire' ]) }}
                        @csrf <!-- cross-site request forgery token / verifies user is authenticated one-->
                        <div class="form-group">
                            {!! Form::label('title', 'Questionnaire Title:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control rounded shadow-sm', 'id' => 'title']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Questionnaire Description:') !!}
                            {!! Form::text('description', null, ['class' => 'form-control rounded shadow-sm', 'id' => 'title']) !!}
                        </div>
                        <!-- hidden form group used to submit active value if checkbox left blank -->
                        <div class="form-group">
                            {!! Form::hidden('active', '0', ['id' => 'active']) !!}
                            {!! Form::checkbox('active', '1', false, ['id' => 'active' ]) !!}
                            {!! Form::label('active', ' Check for Active / Empty for Inactive ') !!}
                        </div>
                        <div>
                            {!! Form::submit('Add Questionanire', ['class' => 'btn btn-success btn-sm rounded shadow-sm']) !!}
                        </div>
                    {{ Form ::close() }}
                </div><!-- Card body -->
            </div><!-- Card -->
        </div><!-- Col -->
    </div><!-- Row -->
</div><!-- Container -->
@endsection
