<!-- page structure shared from main layout -->
@extends('layouts.app')

<!-- content section to be injected into main layout -->
@section('content')

<!-- flash messages for adding or deleting questions  -->
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if(Session::has('flash_message1'))
    <div class="alert alert-danger">
        {{ Session::get('flash_message1') }}
    </div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-2 shadow-sm">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h3 class="col-11 pl-0">{{ $questionnaire->title }}</h3>
                        <!-- inline if statement to show active status based on active value -->
                        <p> {{ $questionnaire->active == 1 ? 'Active' : 'Inactive'}} </p>
                    </div>
                </div>
                <div class="card-body d-flex justify-content-between">
                    <div>
                        <p class="mb-2"><strong>Questionnaire Description: </strong></p>
                        <p>{{ $questionnaire->description }}</p>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-sm btn-info shadow-sm" href="{{ route('create_question', $questionnaire->id) }}">Add Question</a>
                        <a class="btn btn-sm btn-warning shadow-sm" href="{{ route('edit_questionnaire', $questionnaire->id) }}">Update Questionnaire</a>
                        <a class="btn btn-sm btn-dark shadow-sm" href="{{ route('show_survey', $questionnaire->id) }}">Preview</a>
                        {{ Form::model($questionnaire, [ 'route' => ['delete_questionnaire',  $questionnaire->id]]) }}
                            @method('DELETE')
                            @csrf
                            {!! Form::submit('Delete Questionnaire', ['class' => 'btn btn-danger btn-sm rounded shadow-sm']) !!}
                        {{ Form ::close() }}
                    </div>
                </div>
            </div><!-- card -->

            

            @forelse($questionnaire->questions as $question)
                <div class="card mb-4 shadow-sm">
                    <div class="card-header d-flex justify-content-between">
                        <h4 class="col-9">{{ $question->question }}</h4>
                        <form class="d-inline pt-2" action ="{{ route('delete_question', [$questionnaire->id, $question->id]) }}" method="post"> 
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-sm btn-danger mb-0 shadow-sm" type="submit">Delete Question</button>         
                        </form>     
                    </div>

                    <div class="card-body">
                        <ul class="list-group ml-1">
                        
                            @foreach($question->answers as $answer)
                                <li class="list-group-item d-flex justify-content-between">
                                    <div class="col-8 pl-0">{{ $answer->answer }}</div>
                                    @if($question->responses->count())
                                        <div class="col-3">{{ intval(($answer->responses->count() * 100) / $question->responses->count()) }}%</div>
                                    @endif
                                    
                                    <div class="col-2">({{ $answer->responses->count() }})</div>
                                </li>
                            @endforeach
                            <p class="my-3"><strong>Response Total : {{ $question->responses->count() }}</strong></p>
                        </ul>
                    </div>
                </div>
            @empty
                <p class="text-danger text-center mt-3">No Questions yet</p>
            @endforelse
        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->
@endsection
